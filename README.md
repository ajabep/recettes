# Recettes

Il y a des recettes que j'aime bien. Je les notes ici pour en garder une trace
pour moi. Si tu les veux, tu peux aussi y accéder.


**\[EN\]** There are some recipes that I love. I put them here to keep a trace
for myself. If you want them, pick them :) (even if they will probably be in
french).


## Arborescence

* [recettes/](recettes/) : Mes recettes
* [recettes/pas testés/](recettes/pas testés/) : Les recettes que j'ai pu
  récolter sans les avoir (encore) tester.
* [recettes/YOLO/](recettes/YOLO/) : Les recettes que j'ai pu consevoir par
  moi-même, généralement par accident. Aucun état de l'art n'est respecté dans
  ce dossier. Les recettes sont surtout faite pour fonctionner, sans plus.

___

Icon from [Rakhmat Setiawan on iconfinder.com](https://www.iconfinder.com/icons/2048807/pot_restaurant_food_cooking_hot_vegetable_kitchen_icon)

![](img/icon.svg)
