---
testée: oui
temps estimé:
  préparation: 00h 20'
  cuisson: 00h 35'
  repos: 01h 00'
tags: vegan
source: https://www.cuisineaz.com/recettes/tarte-aux-pommes-vegan-99638.aspx
---

# Tarte aux pommes vegan


## Ingrédients

### Pâte
* 250g de farine
* 30g de sucre roux
* 5g de Poudre levante
* 50mL d'huile végétale
* 125mL de lait végétal
* un petit peu de cannelle


### Garniture

* 6 Pommes
* 3 c. à soupe de compote de pomme
* 50 g de Purée d'amande
* 1 c. à soupe de sucre roux


## Recette

1. Mélanger la farine et la poudre levante dans un saladier.
2. Mélanger avec le sucre en poudre et la pincée de cannelle.
3. Ajouter le lait végétal et l’huile.
4. Pétrisser jusqu’à obtention d’une boule de pâte homogène.
5. Abaisser la pâte à tarte sur un plan de travail légèrement fariné.
6. Étaler la pâte dans un moule à tarte et piquer le fond à l’aide d’une fourchette.
7. Préchauffer votre four à 180°C.
8. Peler et épépiner les pommes. Couper-les en lamelles.
9. Étaler la purée d’amandes sur le fond de tarte.
10. Répartisser la compote de pommes.
11. Disposer les lamelles de pommes en rosace.
12. Saupoudrer de sucre roux.
13. Enfourner la tarte aux pommes pendant environ 35 minutes.
14. Faire refroidir la tarte.


## Crédits

Recette trouvée sur https://www.cuisineaz.com/recettes/tarte-aux-pommes-vegan-99638.aspx
