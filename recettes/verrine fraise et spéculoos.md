---
testée: oui
temps estimé:
  préparation: 00h 00'
  cuisson: 00h 00'
  repos: 00h 00'
parts: 4
source: https://youcookcuisine.com/recette/verrine-fraise-et-speculoos/
---

# Verrine fraise et spéculoos

Recette parfaite ! Je l'ai testé sans menthe, car je n'en avais pas.

Ma vaiselle étant plus grande que la leur, il m'a fallut le double de quantité
pour remplir 4 verres.


## Ingrédients

* 500 g de fraises (ou un autre fruit)
* 300 g de fruits rouges congelés
* 250 g de mascarpone
* 100 g de sucre en poudre
* 1 gousse de vanille
* Spéculoos
* Menthe


## Recette

1. Dans une casserole, versez les fruits rouge et le sucre en poudre, puis
   laissez réduire à feu doux environ 10min. Mixez ensuite le tout à l'aide
   d'un blender, puis réservez au frigo.

2. Dans un saladier, déposez le mascarpone, le yaourt nature, le sucre glace
   ainsi que l'intérieur de la gousse de vanille. Mélangez le tout à l'aide
   d'un fouet, puis réservez au frigo.

3. Découpez vos fraises en 4, hachez la menthe finement et réduisez les
   spéculoos en poudre.

4. Au moment de servir, déposez la poudre de spéculoos au fond d'une verrine
   (un grand verre fera parfaitement l'affaire). Disposez ensuite plusieurs
   couches en alternant mascarpone, fraises, menthe et coulis de fruits rouges
   jusqu'à ce que la verrine soit pleine.


## Crédits

Recette prise tel quel depuis le site de
[YouCook Cuisine](https://youcookcuisine.com/recette/verrine-fraise-et-speculoos/).
Elle n'a pas nécessité d'être adaptée.
