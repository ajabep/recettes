---
testée: oui
temps estimé:
  préparation: 10'
  cuisson: 45' -- 1h
source: https://www.youtube.com/watch?v=JUfWcJxGZxY
---

# Patate douce rôtie au four

Très bon.


## Ingrédients

* Autant de patate douce que de personnes
* De l'huile d'olive, de manière généreuse
* Des condiments pour la cuisson (ex: de l'ail)
* Du sel
* Ce que vous souhaitez pour le dressage (suggestions dans la vidéo)


## Recette

1. Arroser le lèchefrite d'huile.

2. Couper les patates douces en deux, et tracer des croisillons assez profond.

3. Condimenter en fonction de vos envies et stocks.

4. Verser un filer d'huile sur les patates, saler

5. Enfourner à 180° pendant 45 minutes à 1 heure.

6. Les patates seront cuites quand elle seront grillées, avec les croisillons
   qui se seront développer.

7. Dresser.


## Crédits

Cette recette viens de la [vidéo de François-Régis Gaudry](https://www.youtube.com/watch?v=JUfWcJxGZxY).
