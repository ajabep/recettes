---
testée: oui
temps estimé:
  préparation: 00h 20'
  cuisson: 0h 10'
  repos: 01h 10'
tags: vegan
source: A friend
---

# Fromage tartinable Vegan

Fromage "frais" tartinable. Lorsque je l'ai fait, il était très aigre, mais je n'avais pas de levure maltée. Cela change probablement le gout final.

## Ingrédients

* Lait de **soja** (important que ce soit du soja)
* Environ 1 cuillere de vinaigre pour 250ml de lait
* Un linge propre pour aigouter le caillé du lait
* Assaisonnement (herbesn, levure malté, poivre, échalotes, etc.)
* Un poil d'huile de coco


## Recette

1. Prendre le lait de soja ;
2. Le faire chauffer ;
3. Quand ça commence à fummer un petit peu, rajouter environ 1 cuillere de vinaigre pour 250ml ;
4. Fouter un coup ;
5. Le laisser reposer une dixaine de minutes ;
6. Faire passer dans un linge, pour essorer ;
7. Le laisser pendre, genre 1h. Ça permet de récupérer le caillé du lait de soja ;
8. L'assaisonner au goût. Par exemple : de la levure malté pour un côté fromagé, poivre, échalotes, etc. ;
9. Pour avoir une texture plus fromagère, mélanger avec un poil d'huile de coco.


## Crédits

N., a friend.
