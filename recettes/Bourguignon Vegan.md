---
testée: oui
temps estimé:
  préparation: 00h 00'
  cuisson: 00h 00'
  repos: 00h 00'
tags: vegan
source: https://cuisine.journaldesfemmes.fr/recette/3098763-boeuf-bourguignon-vegan
---

# Bourguignon vegan

Excellent! Vraiment! 


## Ingrédients

* 300g de tofu fumé
* 5 carottes
* 1 oignon
* 300g de champignons
* 1 echalote
* 1 gousse d'ail
* 2 cuillère à soupe de concentré tomates
* 2 cuillère à soupe de maïzena
* 4 cuillère à soupe de huile
* 60cL de vin rouge
* 30cL d'eau
* Bouquet garni
* Assaisonnement


## Recette

1. Couper les carrottes en rondelles.
2. Couper les chanpignons en lamelles
3. Hacher la gousse d'ail
4. Émincer l'oignin et l'échalote.
5. Dans la marmite, faire revenir l'ail, l'oignon et l'échalote dans 2 c.a.s d'huile.
6. Ajouter le tofu et les champignons.
7. Laisser mijoter à feu doux pendant 10 minutes.
8. Mélanger la fécule de maïs et 2 c.a.s. d'huile jusqu'à obtention d'une texture bien lisse.
9. Ajouter cette texture lisse à la marmite.
10. Verser le vin rouge, l'eau, le concentré de tomate, le bouquet garni et les carottes.
11. Assaisonner.
12. Faire mijoter pendant 45 minutes.
13. Verifier la cuisson des carrotes.


## Crédits

La recette vient [du journal des femmes](https://cuisine.journaldesfemmes.fr/recette/3098763-boeuf-bourguignon-vegan).

Puisque je savais à quoi m'attendre, pour en avoir déjà fait, je n'ai pas regardé les avis. C'est en concignant cette recette que je les ais regardés. C'est mal noté parce que les commentaires sont remplis de larmes de viandards. Cela rends la recette d'autant plus diététique: les larmes de viandards sont pleines de B12 😘

Plus sérieusement, c'est la meilleure recette que j'ai pu trouvé sur le sujet, d'où le fait que je la consigne ici.