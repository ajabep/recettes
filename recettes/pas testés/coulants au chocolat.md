---
testée: non
temps estimé:
  préparation: 10'
  cuisson: 10'
parts: 6
---

# Coulants au Chocolat


## Ingrédients

* 200g de chocolat + 6 carrés
* 3 œufs
* 80 g de sucre
* 70 g de beurre
* 1 cuillerée à soupe de farine


## Recette

1. Préchauffez le four à 240°C
2. Faites fondre le chocolat (mettez 6 carrés de côte) avec le beurre
3. Dans un saladier, mélangez les oeufs et le sucre.
4. Ajoutez la farine puis le chocolat fondu. Mélangez et laissez refroidir
5. Remplissez un moule à muffin à moitié avec la pate.
6. Ajoutez un carré de chocolat et recouvrez du reste de pâte.
7. Faites cuire dans le four dizaine de minutes.


## Crédits

Notice fabriquant d'une tablette de chocolat.
