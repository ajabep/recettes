---
testée: non
temps estimé:
  préparation: 10'
  cuisson: 45'
parts: 6
---

# Quarte-quarts

## Ingrédients

* 3 œufs, séparer les jaunes et les blancs
* Beurre
* Sucre
* Farine
* Sel

Prendre la même masse de beure, sucre et farine que la masse totale des œufs.

## Recette

1. Faire fondre le beure.
2. Monter les blancs d'œufs en neige.
3. Blanchir les jaunes d'œufs avec le sucre.
4. Y verser le beure, la farine et les blancs.
5. Ajouter une pincée de sel.
6. Encourner à 160° pendant 45 mins.


## Crédits


Notice fabriquant d'un moule à quatre-quarts.
