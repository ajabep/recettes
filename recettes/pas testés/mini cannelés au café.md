---
testée: non
temps estimé:
  cuisson: 35'
parts: 30 (mini cannelés)
---

# Mini cannelés au café

## Ingrédients

* 25 cl de lait
* 1 gros œuf
* 1 jaune
* 1 cuillère à café bombée de café lyophilisé
* 50 g de farine
* 15 g de beurre
* 75 g de sucre glace
* 1 gousse de vanille
* 1 cuillère à soupe de rhum


## Recette

1. Faire chauffer le lait avec le beurre, le café lyophilisé et la gousse de
   vanille grattée.
2. En parallèle, mélanger l'oeuf, le jaune et le sucre glace. 
3. Ajouter la farine.
4. Verser dessus progressivement le mix lait/beurre/café chaud.
5. Laisser reposer au frais 12 heures avant de verser dans les empreintes à
   cannelés au $`3/4`$.
6. Préchauffer le four à 210°C.
7. Enfourner pour une cuisson de 10 minutes à 210°C.
8. A ce moment-là, baisser le four à 180°C et poursuivre la cuisson 25 minutes
   sans ouvrir.


## Crédits

Notice fabriquant d'un moule à cannelé.
