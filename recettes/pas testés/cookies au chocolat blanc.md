---
testée: non
temps estimé:
  préparation: 20'
  cuisson: 10 -- 12'
parts: 4
---

# Cookies au chocolat blanc

## Ingrédients

* 75 g de chocolat blanc
* 140 g de farine
* 1 cuillère à café de poudre à lever
* 1 œuf
* 50 cl d'huile d'olive
* 40 g de poudre d'amande
* 80 g de sucre roux
* 1 cuillère à café de thym


## Recette

1. Préchauffer le four a 180°C
2. Hacher grossièrement le chocolat blanc.
3. Mettre le thym dans un mortier avec une cuillère soupe de sucre roux. Piler
   jusqu'à obtention d'une poudre.
4. Mélanger la farine et la poudre à lever.
5. Dans un récipient, battre le sucre, le thym, l'huile d'olive et la poudre
   d'amande.
6. Ajouter l'oeuf, battre à nouveau, puis incorporer le mélange farine poudre à
   lever et le chocolat haché.
7. Prélever un peu de pâte avec une cuillère à café, la déposer sur une plaque
   de cuisson. Répéter l'opération jusqu'a épuisement de la pâte.
8. Aplatir les petits tas de pâte à l'aide d'une fourchette légerement
   mouillée.
9. Enfourner 10 à 12 minutes



## Crédits


Notice fabriquant d'un tapis de cuisson.
