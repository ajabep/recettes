---
testée: non
temps estimé:
  préparation: 20'
  repos: 3h
parts: 6
---

# Mousse an Chocolat

## Ingrédients

* 6 œufs
* 200g de chocolat noir


## Recette

1. Faites fondre le chocolat au bain-marie, laissez tiédir.
2. Ajoutez les jaunes et mélangez énergiquement.
3. Ajoutez une pincée de sel dans les blancs et battez-les en neige.
4. Incorporez délicatement au chocolat $`1/3`$ des blancs d'œufs battus, puis
   ajoutez le reste délicatement en soulevant la préparation de bas en haut.
5. Faites prendre la mousse 3h minimum au réfrigérateur.


## Crédits

Notice fabriquant d'une tablette de chocolat.
