---
testée: non
temps estimé:
  préparation: 10'
  cuisson: 10'
parts: 8
---

# Fondant an Chocolat

## Ingrédients

* 200g de chocolat
* 4 œufs
* 100 g de beurre
* 100 g de sucre
* 50 g de farine


## Recette

1. Préchauffez le four à 200°C.
2. Faites fondre le chocolat et le beurre.
3. Ajoutez le sucre, les oeufs, la farine. Mélangez bien.
4. Versez la pâte dans le moule.
5. Faites cuire au four une dizaine de minutes.

Note: À la sortie du four, le gateau ne paraît pas assez cuit. C'est normal,
      laissez-le refroidir avant de le démouler.


## Crédits

Notice fabriquant d'une tablette de chocolat.
