---
testée: oui
temps estimé:
  préparation: 00h 15'
  cuisson: 00h 50'
  repos: 00h 00'
tags: vegan
parts: 15 minis cannelés (je n'ai que ce moule)
source: https://francevegetalienne.fr/blog/2017/6/29/canels-ou-cannels-vgtalien-vegan
---

# Cannelés Vegans

Super bons!


## Ingrédients

* 50 g de farine de blé
* 100 g de cassonade
* 10 cL de lait d'amande (ou autre lait végétal, avoine, soja...)
* 6 cL de crème végétale
* 4 cL d'huile neutre
* 1 c.a.c de vanille liquide
* 2 c.a.s de rhum


## Recette

1. Mélanger la farine et le sucre dans un saladier.
2. Y verser le lait végétal en mélangeant avec un fouet, puis la crème, l'huile, la vanille et le rhum. 
3. Remplir le moule de la pâte jusqu'à 1 ou 2 millimètres du bord.
4. Si votre four chauffe uniquement du haut, placer vos canelés tout en bas, sinon au milieu.
5. Enfourner dans un four préchauffé à 240°C, pendant 10 minutes.
6. Puis, baisser à 180°C et laisser encore 40-50 minutes selon votre four (vérifier si au bout de 40 minutes ça ne brûle pas). La croûte doit prendre une teinte légèrement brune.
7. Attendre le refroidissement complet avant de démouler.
8. Garder vos gâteaux dans un recipient avec un couvercle pour qu'ils ne dessèchent pas.


## Crédits

Cette recette viens de https://francevegetalienne.fr/blog/2017/6/29/canels-ou-cannels-vgtalien-vegan.
