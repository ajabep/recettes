---
testée: oui
temps estimé:
  préparation: ±15'
  cuisson: ±10'
  repos: plusieurs jours/mois/etc.
source: https://www.youtube.com/watch?v=IM1ttMHkats
---

# Pickles de rhubarbe

Peut être utilisé en condiments, à la place de cornichons. On notera sa saveur
aigre-douce.

Peut être détournée sur tous un certain nombre de légumes racines.


## Ingrédients

* Pour 200g de rhubarbe (masse une fois préparée ; produit un pot de taille
  moyenne)
* 50g de sucre
* 100g volumes de vinaigre
* 150g volumes d'eau
* Des condiments au choix


## Recette

1. Préparer la rhubarbe (enlever les pieds, retirer les fibres, etc.)

2. Fondre le sucre, le vinaigre et l'eau sur le feu

3. Débiter sa rhubarbe en petite bouchée.

4. Condienter à sa guise la rhubarbe dans le contenant final (style pot de
   confiture)

5. Une fois que le mélange sucre-vinaigre-eau frémit et commence à vous
   aggriper le nez, le verser dans le contenant.

6. Une fois refroidi, réserver au frais jusqu'à consomation.


## Crédits

Cette recette viens de la [vidéo de François-Régis Gaudry](https://www.youtube.com/watch?v=IM1ttMHkats).
