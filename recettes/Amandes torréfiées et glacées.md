---
testée: oui
source: "Le grand manuel du cuisinier" par Marianne Magnier Moreno, Edition Marabout
---

# Amandes torréfiées et glacées

> La torréfaction est l'action d'exposer un aliment à un feu direct ou à une
> source de chaleur adaptée. Elle donne un arôme qui rappelle l'odeur des
> aliments un peu grillés, calcinés.
> -- [Wikipédia](https://fr.wikipedia.org/wiki/Torr%C3%A9faction)


## Ingrédients

* 60g d'amandes effilées
* 30g de beurre
* 15g de sucre


## Recette

1. Dans une poêle, à feu moyen, à sec, faire griller les amandes, en mélangeant
   régulèrement, jusqu'à obtenir une couleur brune.
2. Faire fondre le beurre et le sucre dans une poêle à feu moyen.
3. Mélanger avec les amandes.


## Crédits

Tiré de la recette des choux de Bruxelles glacés, dans "Le grand manuel du cuisinier" par Marianne Magnier Moreno, Edition Marabout
