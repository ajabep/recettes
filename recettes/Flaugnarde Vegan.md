---
testée: oui
temps estimé:
  préparation: 00h 15'
  cuisson: 45' - 1:00
tags: vegan, flaugnarde
parts: 10-15
source: https://auvertaveclili.fr/clafoutis-aux-pommes-et-aux-poires-sans-gluten-vegan/
---

# Flaugnarde Vegan

Bien bon. C'est normal si la preparation est mélangée aux fruits. Bien faire attention aux grumaux et ne pas aller trop vite. Je n'ai pas ajouté de sel, ça ne m'a pas paru étrange. Je l'ai donc retiré de ces notes.

Pour le double de personnes, rajouter le double de préparation, ça marche bien!


## Origine

Ce désert consiste en une préparation similaire au flan mélangé avec des fruits. Il s'agit d'une recette assez comune en France, avec de nombreuses variation régionales. Avec des cerices, il s'agit d'un "clafoutis", venant d'Occitanie (sud-ouest, avec Toulouse), avec de la prune ou du raisin, c'est un "Far Breton", de Bretagne, et avec de la pomme, c'est une "Flamusse Bourguignonne", venant de Bourgogne (centre-est, avec Dijon).

Le nom vient des mots Occitans (langue originaire d'Occitanie, sud-ouest, avec Toulouse) "Fleunhe" et "Flaunhard", pouvrant être traduit par "doux" ou "duveté".


## Alergènes

* Soja
* Maïs
* Riz
* Fruits
* Pomme


## Ingrédients

* 4 poires (pour une flaugnarde à la poire);
* 100g de farine de riz;
* 50g de fécule de maïs;
* 80g de sucre de canne;
* 400ml de lait végétal de soja;
* 3 cuillère à soupe de compote de pommes sans sucre ajouté;
* 1 cuillère à café d'extrait de vanille;
* 1 cuillère à soupe de margarine végétale.


## Recette

1. Découper les fruits sans peau en tranches fines.
2. Mélanger la farine de riz, la fécule de maïs et le sucre.
3. Ajoutez progressivement le lait végétal et la compote de pomme, en remuant constamment pour éviter les grumeaux.
4. Incorporez l’extrait de vanille.
5. Graissez un plat à gratin avec la margarine végétale. Un couche de sucre en poudre est aussi faisable pour le rendre encore moins acrochant, mais pas obligatoire.
6. Disposez les tranches de fruit uniformément dans le plat.
7. Versez la préparation liquide sur les fruits.
8. Enfournez pendant 40-45 minutes, à 180°C, jusqu’à ce que le clafoutis soit doré et légèrement ferme au toucher.


## Crédits

Cette recette a été trouvée sur https://auvertaveclili.fr/clafoutis-aux-pommes-et-aux-poires-sans-gluten-vegan/.
