---
testée: oui
temps estimé:
  préparation: 00h 10'
  cuisson: 00h 20'
tags: vegan
parts: une douzaine de gaufres
source: https://patateetcornichon.com/recettes/gaufres-vegan
---

# Gaufre Vegan

Les meilleurs gaufres que j'ai pu faire dans ma vie ❤️

Dès le départ, la recette était parfaite.


## Ingrédients

* 210g de farine
* 50g de sucre
* 0.5g (+/- 1 sachet) de levure chimique
* 290mL de lait de soja
* 1 gousse vanille
* 40g de margarine végétale


## Recette

1. Verser la farine, le sucre et la levure ;
2. Fouetter le tout ;
3. Ajouter le lait de soja progressivement en fouettant pour obtenir une pâte lisse ;
4. Faire fondre la margarine et la verser dans la préparation ;
5. Terminer par rajouter les grains de vanille de la gousse ;
6. La pâte à gaufre est prête !


## Crédits

Recette trouvée sur https://patateetcornichon.com/recettes/gaufres-vegan
