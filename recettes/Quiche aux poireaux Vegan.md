---
testée: oui
temps estimé:
  préparation: 00h 30'
  cuisson: 00h 20' + 00h 30'
  repos: 00h 00'
tags: vegan
parts: 1 tarte
source: https://www.lafeestephanie.com/2015/09/quiche-aux-poireaux-recette-vegetalienne.html
---

# Quiche aux poireaux Vegan

Très bon 👌


## Ingrédients

* 1 kg de poireaux
* 300 g de tofu soyeux
* huile d’olive pour la cuisson
* sel
* poivre
* noix de muscade
* curcuma


## Recette

1. Couper finement les poireaux.
2. Faire chauffer un peu d’huile d’olive et rajouter les poireaux.
3. Saler, poivrer et faire mijoter une vingtaine de minutes à feu doux, jusqu’à ce que les poireaux soient bien tendres.
4. Ajouter la noix de muscade.
5. Dans le bol du mixeur, verser le tofu soyeux, le curcuma, une pincée de sel et de poivre et mixer jusqu’à obtenir une crème homogène et lisse.
6. Verser le mélange obtenu sur les poireaux et mélanger.
7. Verser les poireaux dans le moule et les répartir uniformément sur la pâte.
8. Ajouter un peu de poivre moulu et enfourner pendant 30 minutes à 220°.
9. Le temps de cuisson peut dépendre d’un four à l’autre et de l’épaisseur de votre tarte. Surveiller la cuisson et sortir du four quand la tarte sera bien dorée.


## Crédits

Bravo à La Fée Stéphanie pour [cette recette](https://www.lafeestephanie.com/2015/09/quiche-aux-poireaux-recette-vegetalienne.html)! Elle est validée!
