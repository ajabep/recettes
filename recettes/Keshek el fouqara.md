---
testée: oui
temps estimé:
  préparation: 00h 30'
  cuisson: 00h 00'
  repos: 7 semaines minimum.
tags: vegan, fromage
source: https://www.lafeestephanie.com/2022/05/fromage-de-boulgour-fermente-specialite-libanaise.html
---

# Keshek el fouqara, un fromage de boulgour

Super fromage vegan! Je vie avec une dévoreuse de fromage, et elle s'est régalé avec ceux-là !

Désormais, j'en ai plusieurs pôts, et je ne compte pas m'arrêter-là!


## Ingrédients

* 500g de boulgour
* 2L d'eau
* 30g de sel
* Assaisonnement


## Recette

1. Bien rincer le boulgour sous l'eau claire
2. Égoutter
3. Répartir le boulgour dans des bocaux. Puisque le boulgour va gonfler, veiller à ne pas les remplir à plus des 2 tiers.
4. Diluer complètement le sel dans l'eau.
5. Verser l'eau sur le boulgour. La quantité d'eau peut sembler importante, mais le boulgour va énormément gonfler. Celui-ci doit toujours être couvert abondamment d'eau. Rajouter de l'eau avec la même concentration de sel si besoin.
6. Couvrir les bocaux d'une compresse de gaze ou d'un linge fin et d'un élastique pour permettre à l'air de passer.
7. Les placer dans un endroit sombre et frais.
8. Pendant 3 semaines, les mélanger quotidiennement pour éviter la formation de moisissures. Une ligne blanche va se former, c'est normal. Au fur et à mesure, une légère odeur un peu acidulée va se dégager, c'est le boulgour qui fermente. Si le boulgour sort du niveau de l'eau, rajouter de l'eau avec la même concentration de sel si besoin.
9. Au bout de 3 semaines, filtrer l'eau
10. Laisser le boulgour égoutter dans une passoire pendant 12 heures en remuant de temps de temps.
11. Faire des petites boules en roulant une cuillerée de farce entre les mains.
12. Faire sécher les fromages pendants quelqies jours à température ambiante. Retourner les fromages de temps en temps.
13. Une fois qu'une légère croûte s'est formée, répartir les fromages dans des bocaux stérilisés et couvrir complètement d'huile d'olive extra vierge ou garder les fromages au frais pendant 1 mois. Ils vont durcir et prendront la consistance du parmesan.
14. Les bocaux se conservent jusqu'à un an dans un endroit sombre, à l'abri de la chaleur. Une cave serait idéale, sinon le placard de la cuisine (n'ayant pas de cave, c'est obligé pour moi), mais ne mettez pas l'huile au frigo 🙂


## Crédits

Bravo à La Fée Stéphanie pour [cette recette](https://www.lafeestephanie.com/2022/05/fromage-de-boulgour-fermente-specialite-libanaise.html)! Elle est validée!
