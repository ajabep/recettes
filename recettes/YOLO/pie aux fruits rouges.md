---
testée: oui
temps estimé:
  préparation: 30'
  cuisson: 10' puis 50'
parts: N/A
régime alimentaire: végétarien, voire vegan si vous faites la pâtes
allergènes:
  incontournables: amandes, soja
  remplacables: fruits rouges, gluten, lait
---

# Pie aux fruits rouges

Peut être adapté avec les fruits que vous voulez.

Viens du fait de devoir vider mon congélateur.


## Ingrédients

* 20cl de crème de soja
* 180g de poudre d'amandes
* 180g de sucre
* 450g de fruits rouges
* 2 pates (suggestion: sablée et feuilletée)
* du sucre et des amandes effilées pour le dressage.


## Recette

1. Mettre la pate de base (sablée) au four 10 minutes;
2. Mélanger la poudre d'amandes, le sucre, les fruits rouges, et la crême de soja;
3. Une fois la pate de base pré-cuite, verser tout à l'intérieur;
4. Couvrir de la dernière pate. Arroser de sucre, mettre des amandes, et du jus des fruits incorporés;
5. Mettre à cuire 50 à 60 minutes;
6. Se régaler.


## Crédits

Just me, and probably a lot of other cookers.
