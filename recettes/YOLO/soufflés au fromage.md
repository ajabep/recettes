---
testée: oui
temps estimé:
  préparation: 1'
  cuisson: 30' + 10' optionnelles
parts: 1
---

# Soufflés au fromage

Peut être adapté avec ce que vous souhaitez.

Viens du fait de vouloir savoir ce que donnait deux œufs au four.

A vos risques et périls !


## Ingrédients

* 2 œufs
* Du fromage (autant que vous en voulez, pas plus que d'œufs), ou ce que vous
  souhaitez à la place.


## Recette

1. Battre les deux œufs en ommelettes.
2. Y intégrer la garniture (fromage ou autre).
3. Le verser dans un ramequin.
4. L'enfourner pendant 30 minutes au four à 180°
5. Vous pouvez les laissers 10 minutes de plus pour bien les dorés et les
   gonflés.
6. Laissez refroidir quelques instants et régalez-vous


## Crédits

Recette inventée de toutes pièces par moi-même.
