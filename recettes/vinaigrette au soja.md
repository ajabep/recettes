---
testée: oui
temps estimé:
  préparation: 1'
source: https://www.youtube.com/watch?v=PFx07r6M-jA
---

# Vinaigrette au soja

Très bonne vinaigrette, rafraichissante l'été et surprenante lors de diners.

## Ingrédients

* 1 volume de sauce soja salée
* 1 volume de vinaigre sucré (ex: balsamique)
* 1 volume de vinaigre plus classique (ex: de Xeres, de vin rouge)
* 6 volumes d'huile


## Recette

1. Mélanger le tout, émulsionner le mélange.


## Crédits

Cette recette viens du livre "Le livre de cuisine" de Andrée Zana Murat.

Je l'ai découvert avec la [vidéo de François-Régis Gaudry](https://www.youtube.com/watch?v=PFx07r6M-jA).
