---
testée: oui
temps estimé:
  préparation: 00h 15'
  cuisson: 00h 30'
  repos: 01h 00'
tags: vegam
parts: Une douzaine de graufres
source: https://patateetcornichon.com/recettes/crepes-sucrees-vegan-sans-lait-et-sans-oeufs
---

# Crêpes Vegans au pain perdu

La recette de base est excellente! Comme tout ce que fait Patate et Cornicon. Neanmoins, j'ai découvert la farine de pain dûr, et ai voulu tester. Ma femme valide le goût mélangeant crêpe et pain perdu... d'où le nom :)

De ce que j'ai pu trouvé la farine de pain dûr s'emploi en remplaçant la farine de blé par 3/5 de "farine de pain dûr" (pain dûr mixé finement) et le reste (2/5) de farine de blé.

Néanmoins, ma pâte finale était trop épaisse, trop rugueuse pour laisser la vapeur s'echaper, produisant donc beaucoup de bules sous la crêpe. Peut-être est-ce la faute à cette nouvelle farine. Probablement, même.

Enfin, j'ai aussi explicité de delayer la pâte, car j'oublie régulièrement cette étape.


## Ingrédients

* 30g de fécule de maïs ;
* 650mL de lait de soja ;
* 180g de farine de pain dûr (180g de pain dûr réduit en poudre avec un mixeur) ;
* 120g de farine de blé ;
* 1 pincée de sel ;
* 1 cuilère à soupe d'huile neutre ;
* 50g de sucre ;
* De l'eau de fleur d'oranger


## Recette

1. Mélanger les farines, la fécule, le sucre, le sucre vanillé et la pincée de sel.
2. Verser le lait de soja progressivement en mélangeant rapidement avec un fouet.
3. Dès que la pâte est liquide, ajouter le rhum ou l'eau de fleur d'oranger et la cuillère à soupe d'huile.
4. Délayer la pâte avec un peu d'eau pour rendre la texture vraiment fluide.
5. Si possible, laisser la pâte reposer une heure environ.
6. Faire chauffer ta poêle légèrement huilée à feu vif.
7. Verser une louche de pâte et recouvrir la poêle avec.
8. Décoller la crêpe avec une spatule en vérifiant qu'elle soit suffisamment dorée. Pour les autres crêpes, fais les cuire sur feu moyen.


## Crédits

Recette de base trouvée sur https://patateetcornichon.com/recettes/crepes-sucrees-vegan-sans-lait-et-sans-oeufs
