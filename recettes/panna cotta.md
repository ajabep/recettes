---
testée: oui
temps estimé:
  préparation: ??
  cuisson: ??
  repos: 2h à 4h
parts: 8
source: https://youcookcuisine.com/recette/panna-cotta-aux-fruits-rouges/
---

# Panna cotta aux fruits rouges

La recette est parfaite.

Les seules modifications que j'ai effectué en testant étaient de prendre de la
gélatine au lieu de l'agar agar parce que je n'en avais pas. Il me semble que
j'en avais pris 2-3 feuilles allant de 200 à 225
[blooms](https://fr.wikipedia.org/wiki/Degr%C3%A9_Bloom).

J'ai aussi testé la recette avec du lait de riz, elle a très bien fonctionné.

Ma vaiselle étant plus grande que la leur, il m'a fallut plus de quantité pour
remplir 8 verres.


## Ingrédients

* 80 cl de crème liquide 30%
* 120 g de sucre (séparé en $`2/3`$ - $`1/3`$)
* 4 g d'agar agar
* 20 g de lait
* 250 g de framboises (ou autre fruit de votre choix)
* 1 demi citron
* Un peu d'arôme de vanille


## Recette

1. Dans une casserole, versez la crème liquide, les $`3/4`$ de sucre et l'arôme
   de vanille. Chauffez à feu doux et mélangez constamment. Lorsque la
   préparation commence à bouillir, coupez le feu et laissez infuser à couvert.

2. Dans un petit récipient, diluez l'agar agar dans un tout petit peu de lait
   froid en mélangeant bien, puis incorporez la préparation dans la casserole
   de crème à feu doux. Mélangez constamment avec un fouet.
   Lorsque la préparation commence à bouillir, comptez une minute avant de la
   retirer du feu.
   Versez la préparation dans des verrines ou dans des moules en silicones.
   Mettez les verrines au frigo pendant 2 à 4 heures.

3. Pendant ce temps, place au coulis !
   Placez dans une casserole les framboises, le $`1/3`$ restant de sucre et le
   jus de citron.
   Faites chauffer à feu doux en mélangeant.
   Un fois prêt, vous pouvez le garder tel quel ou bien le passer au chinois
   pour obtenir un coulis très lisse.
   Placez le au frigo.

4. C'est l'heure du dressage !
   Versez le coulis sur la panna cotta et déposez quelques fruits entiers ou
   coupés pour la décoration (et la gourmandise).


## Crédits

Recette prise tel quel depuis le site de
[YouCook Cuisine](https://youcookcuisine.com/recette/panna-cotta-aux-fruits-rouges/).
Elle n'a pas nécessité d'être adaptée.
