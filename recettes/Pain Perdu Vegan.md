---
testée: oui
temps estimé:
  préparation: 00h 05'
  cuisson: 00h 05'
  repos: 00h 10'
tags: vegan
source: https://vegan-pratique.fr/recettes/pain-perdu/
---

# Pain perdu vegan

Bien penser à le laisser tremper en entier.


## Ingrédients

* Du pain dûr, en tranche de 2 cm environ
* 250 ml de lait de soja adouci ou aromatisé à la vanille
* 1 c. à café d’arôme liquide de fleur d’oranger ou de vanille
* 1 c. à café de cannelle en poudre (optionnel)
* 2 c. à soupe de maïzena
* 1 c. à soupe de sucre en poudre
* 1 c. à soupe de margarine végétale
* 1 c. à soupe de sucre pour saupoudrer à la fin


## Recette

1. Diluer la maïzena dans un peu de lait.
2. Ajouter et mélanger le sucre, le reste de lait et l’arôme de fleur d’oranger (et la cannelle).
3. Faire tremper les tranches de pain 10 min en les retournant de temps en temps.
4. Dans une poêle, faire chauffer la margarine à feu moyen. Faire dorer les tranches en les retournant régulièrement.
5. Saupoudrer de sucre. 


## Crédits

Recette trouvé sur https://vegan-pratique.fr/recettes/pain-perdu/.
